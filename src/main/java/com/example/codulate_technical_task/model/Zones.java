package com.example.codulate_technical_task.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.List;

@org.springframework.data.mongodb.core.mapping.Document(collection = "zones")
public class Zones {
    @Id
    private String id;
    private List<Coordinates> coordinates;
    @Indexed(unique = true)
    private String name;

    public List<Coordinates> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
