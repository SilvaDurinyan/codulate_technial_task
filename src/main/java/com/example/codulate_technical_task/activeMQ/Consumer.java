package com.example.codulate_technical_task.activeMQ;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class Consumer {
    private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;

    public static void consumer(String userId, String text, String topicName) throws JMSException {
        ConnectionFactory factory = new ActiveMQConnectionFactory(url);
        Connection connection = factory.createConnection();
        connection.setClientID(userId);

        try {
            connection.start();
        } catch (Exception e) {
            System.out.println("Connection Fail");
        }
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Topic topic = session.createTopic(topicName);
        MessageConsumer consumer = session.createDurableSubscriber(topic, "SUB" + userId);
        MessageListener listener = new MessageListener() {
            @Override
            public void onMessage(Message message) {
                try {
                    if (message instanceof TextMessage) {
                        ((TextMessage) message).setText(text);
                        TextMessage text = (TextMessage) message;
                    }
                } catch (Exception e) {
                }
            }
        };
        consumer.setMessageListener(listener);
        //connection.close();
    }
}
