package com.example.codulate_technical_task.constants;

public class Constants {
    public static final String OK = "Success";
    public static final String ERROR = "Error";
    public static final String DUPLICATE_VALUE = "duplicate value";
    public static final String ERROR_MATCH = "Characters length doesn’t match";
}
