package com.example.codulate_technical_task.restController;


import com.example.codulate_technical_task.service.CRUDModel.ZonesDAO;
import com.example.codulate_technical_task.service.ZonesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
    private final ZonesService zonesService;

    @Autowired
    public AdminController(ZonesService zonesService) {
        this.zonesService = zonesService;
    }

    @PostMapping("/create_zone")
    public String createZone(@Valid @RequestBody ZonesDAO create) {
        return zonesService.create(create);
    }
}
