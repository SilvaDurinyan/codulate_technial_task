package com.example.codulate_technical_task.restController;

import com.example.codulate_technical_task.model.Coordinates;
import com.example.codulate_technical_task.service.ZonesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final ZonesService zonesService;

    @Autowired
    public UserController(ZonesService zonesService) {
        this.zonesService = zonesService;
    }

    @PostMapping("/send-coordinates")
    public String checkCoordinates(@Valid @RequestBody Coordinates input) {
        return zonesService.checkUserCoordinates(input);
    }
}
