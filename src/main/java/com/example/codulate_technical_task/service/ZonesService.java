package com.example.codulate_technical_task.service;

import com.example.codulate_technical_task.activeMQ.Consumer;

import com.example.codulate_technical_task.constants.Constants;
import com.example.codulate_technical_task.model.Coordinates;
import com.example.codulate_technical_task.model.Zones;
import com.example.codulate_technical_task.repository.ZonesRepository;
import com.example.codulate_technical_task.service.CRUDModel.ZonesDAO;
import com.example.codulate_technical_task.service.polygon.Polygon;
import com.mongodb.DuplicateKeyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import java.util.List;

@Service
public class ZonesService {

    private final ZonesRepository zonesRepository;
    private static String userId = String.valueOf(System.currentTimeMillis());
    private static String topic;
    private static String message;


    @Autowired
    public ZonesService(ZonesRepository zonesRepository) {
        this.zonesRepository = zonesRepository;
    }

    public String create(ZonesDAO create) {
        if (!validation(create)) {
            return Constants.ERROR_MATCH;
        }
        Zones zones = new Zones();
        zones.setCoordinates(create.getCoordinates());
        zones.setName(create.getName());
        try {
            zonesRepository.save(zones);
            return Constants.OK;
        } catch (DuplicateKeyException e) {
            return Constants.DUPLICATE_VALUE;
        } catch (Exception e) {
            return Constants.ERROR;
        }
    }

    public String checkUserCoordinates(Coordinates userCoordinates) {
        try {
            Zones zone = Polygon.checkZones(userCoordinates, getAllZones());
            if (zone == null) {
                topic = "coordinates";
                message = "Coordinates: X = " + userCoordinates.getX() + ", Y = " + userCoordinates.getY() + ", Z = " + userCoordinates.getZ();
            } else {
                topic = "violations";
                message = "Zone " + zone.getName() + " violated";
            }
            Consumer.consumer(userId, message, topic);
            return message;
        } catch (JMSException e) {
            e.printStackTrace();
            return Constants.ERROR;
        }
    }

    private List<Zones> getAllZones() {
        return zonesRepository.findAll();
    }

    private Boolean validation(ZonesDAO zones) {
        if ((zones.getCoordinates() != null && zones.getCoordinates().size() < 3) || zones.getName().length() < 3) {
            return false;
        }
        return true;
    }
}
