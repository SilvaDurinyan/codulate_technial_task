package com.example.codulate_technical_task.service.polygon;

import com.example.codulate_technical_task.model.Coordinates;
import com.example.codulate_technical_task.model.Zones;

import java.util.List;

public class Polygon {

    public static Zones checkZones(Coordinates coordinates, List<Zones> zone) {
        for (Zones zones : zone) {
            if (checkIsCoordinatesInPolygon(coordinates, zones.getCoordinates())) {
                return zones;
            }
        }
        return null;
    }

    private static Boolean checkIsCoordinatesInPolygon(Coordinates coordinates, List<Coordinates> polygon) {
        boolean checkcoordinates = false;
        int j = polygon.size() - 1;

        for (int i = 0; i < polygon.size(); i++) {
            if ((((polygon.get(i).getY() < coordinates.getY()) && (coordinates.getY() < polygon.get(j).getY())) || ((polygon.get(j).getY() <= coordinates.getY()) && (coordinates.getY() < polygon.get(i).getY()))) &&
                    (coordinates.getX() > (polygon.get(j).getX() - polygon.get(i).getX()) * (coordinates.getY() - polygon.get(i).getY()) / (polygon.get(j).getY() - polygon.get(i).getY()) + polygon.get(i).getX()) &&
                    (polygon.get(i).getZ() > coordinates.getZ()))
                checkcoordinates = !checkcoordinates;
            j = i;
        }
        return checkcoordinates;
    }
}
