package com.example.codulate_technical_task.service.CRUDModel;

import com.example.codulate_technical_task.model.Coordinates;

import javax.validation.constraints.NotNull;
import java.util.List;

public class ZonesDAO {

    @NotNull
    private String name;
    @NotNull
    private List<Coordinates> coordinates;


    public List<Coordinates> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Coordinates> coordinates) {
        this.coordinates = coordinates;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
