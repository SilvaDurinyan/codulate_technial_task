package com.example.codulate_technical_task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodulateTechnicalTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodulateTechnicalTaskApplication.class, args);
    }

}
