package com.example.codulate_technical_task.repository;

import com.example.codulate_technical_task.model.Zones;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ZonesRepository extends MongoRepository<Zones, String> {
}
